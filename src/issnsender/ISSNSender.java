/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package issnsender;

/**
 *
 * @author Diego
 */




import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 *
 * @author Diego
 */
public class ISSNSender {
    static final String[] MESES     = {"","Enero","Febrero","Marzo", "Abril","Mayo","Junio","Julio","Agosto","Septiempre","Octubre", "Noviembre","Diciembre"};    
   
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yMMddHHmmss");
            
            String logFileName = String.format("ISSNSender_%s.log",sdf.format(new Date()));
            PrintStream console = System.err;
            FileOutputStream fos = new FileOutputStream(logFileName);            
            System.setErr(new PrintStream(fos));
                
            String fileZip;
            Scanner scanner = new Scanner(System.in);
            do{
                System.out.println("Ingrese el nombre del archivo ZIP que contiene los recibos");
                fileZip = scanner.nextLine();
            } while("".equals(fileZip));
            
              
            
            Pattern pattern = Pattern.compile("^(\\d+)_(\\d{6})_(\\d+).*\\.pdf$");
            ExecutorService executor = Executors.newFixedThreadPool(10);            
                        
            SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
            Date inicio = new Date();
            
            System.out.println("Escribiendo errores a: " + logFileName);            
            System.out.println("Inicio: " + df.format(inicio));
            
            Integer cant   = 0, 
                    cantOk = 0;
           
            ArrayList<Future<Boolean>> futures = new ArrayList();
            ArrayList<Correo> correos = new ArrayList();
            
           
            System.out.println("Descomprimiendo archivos");
            String tempDir = descomprimirArchivo(fileZip);
            System.out.println("Procesando Recibos");
            File directorio = new File(tempDir);
            for(String fileName: directorio.list()) {
                //String fileName = "";
                
                try {                    
                    Matcher matcher = pattern.matcher(fileName);
                    if(!matcher.matches()) throw new FormatException("Error en el formato del nombre");               
                    matcher.reset();
                    
                    if (!matcher.find()) {
                        continue;
                    }
                    cant ++;
                    String cuil = matcher.group(1);
                    Integer periodo = Integer.parseInt(matcher.group(2));
                    Integer tipo = Integer.parseInt(matcher.group(3));

                    // PROCESAR ARCHIVO
                    String mail   = construirMail(cuil);                                                
                    Correo correo = construirCorreo(cuil, periodo, tipo);

                    String adjunto = directorio.getAbsolutePath() + File.separatorChar + fileName;

                    correo.addAdjunto(adjunto);
                    System.out.print("\rArchivos Procesados: " + cant);

                    Future<Boolean> future = executor.submit(new MailTask(mail, correo));
                    futures.add(future);
                    correos.add(correo);   

                } catch (FormatException ex){
                    System.err.println(String.format("El archivo %s no cumple con el formato establecido, no se enviará el recibo: %s\n\t", fileName, ex.getMessage()));
                }                
            }
            System.out.println("");
            executor.shutdown();
            
            int j = 0;
            while (!executor.isTerminated()) {
                System.out.print("\rEsperando que finalice el envío de mails" + repeat(".",j));               
                j = (++j % 4);
                for (int i = 0;i<futures.size();i++) {
                    Future<Boolean> future =  futures.get(i);
                    Correo correo = correos.get(i);
                    if (!future.isDone()) continue;
                    try {
                        Boolean todoOk = future.get();
                        if (todoOk) {
                            for(String adj: correo.getAdjuntos()){
                                eliminarArchivo(adj);
                            }                            
                        }
                    } catch (InterruptedException ex) {
                        Logger.getLogger(ISSNSender.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ExecutionException ex) {
                        Logger.getLogger(ISSNSender.class.getName()).log(Level.SEVERE, null, ex);
                    }                        
                }
            } // Esperamos a que finalicen los threads
            System.out.println("");
                        
            for (int i = 0;i < futures.size();i++){
                Future<Boolean> future =  futures.get(i);
                try {
                    Boolean todoOk = future.get();
                    if (todoOk) {
                        cantOk++;            
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(ISSNSender.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ExecutionException ex) {
                    Logger.getLogger(ISSNSender.class.getName()).log(Level.SEVERE, null, ex);
                }                        
            } 
            
            System.out.println("");
            Date fin = new Date();
            System.out.println("Fin: "+ df.format(fin));
            System.out.println(String.format("Se leyeron %d archivos. Se enviaron %d recibos.", cant, cantOk));
            //float tiempo = (fin.getTime() - inicio.getTime()) / 1000;            
            System.setErr(console);
        } catch (IOException ex) {
            Logger.getLogger(ISSNSender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private static String descomprimirArchivo(String fileZip){
        ZipInputStream zis;
        String tempDir;
        try {
            tempDir = new File(fileZip).getParent() + File.separatorChar + "unzipFiles";                
            FileInputStream fis = new FileInputStream(fileZip);
            
            
            
            zis = new ZipInputStream(fis);                
        } catch (FileNotFoundException ex) {
            System.err.println("No se encuentra el archivo");
            System.exit(1);
            return null;
        }

        ZipEntry fileEntry;
        crearDirectorio(tempDir); 
        try {
            fileEntry = zis.getNextEntry();
            if(fileEntry == null) System.out.println("Error al leer el archivo zip");
            while(fileEntry != null){

                if(fileEntry.isDirectory()) {
                    System.out.println("Leyendo directorio " + fileEntry.getName());
                    fileEntry = zis.getNextEntry();
                    continue;
                }
                String filePath = fileEntry.getName();
                String fileName = getNombreArchivo(filePath);
                String outputName = tempDir + File.separator +  fileName;
                String adjunto = descomprimirArchivo(zis, outputName );
               
                if (adjunto==null) throw new FormatException("Error al extraer el archivo");
               
                fileEntry = zis.getNextEntry();
            }
            
            zis.close();            
        } catch (IOException ex) {
            Logger.getLogger(ISSNSender.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch ( FormatException  ex) {
            Logger.getLogger(ISSNSender.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
              
          
        return tempDir;
    }
    private static Correo construirCorreo(String cuil, Integer periodo, Integer tipo ) throws FormatException{
        Correo correo = new Correo();
        Integer anio = periodo / 100;
        Integer mes = periodo % 100;

        if ( anio < 2000 || anio >= 2100) throw new FormatException("El formato del año es incorrecto: " + anio);
        if ( mes < 1 || mes >= 12) throw new FormatException("El formato del mes es incorrecto:" + mes);

        String asunto = construirAsunto(anio, mes, tipo);
        String texto  = construirCuerpo(anio, mes, tipo);
        
        correo.setAsunto(asunto);
        correo.setMensaje(texto);        
        return correo;
    }

    private static String construirMail(String cuil) throws FormatException {
        if (cuil.length() != 11) throw new FormatException("La longitud del CUIL es incorrecta");
        String uid = cuil.substring(2, cuil.length() - 1);
        if (uid.trim().equals("")) throw new FormatException("CUIL Incorrecto");
        uid = eliminarCeros(uid);
        return String.format("%s@ce.neuquen.gov.ar", uid);        
    }    
     private static String construirAsunto(Integer anio, Integer mes, Integer id_tipo) {
        String tipo = getTipoLiquidacion(id_tipo);
        return String.format("Recibo de sueldo liquidación %s %s %d - %04d%02d%02d", tipo, MESES[mes], anio, anio, mes, id_tipo);
     }
     private static String construirCuerpo(Integer anio, Integer mes, Integer id_tipo){
        String tipo = getTipoLiquidacion(id_tipo);
        return String.format("Se adjunta el recibo de sueldo correspondiente a la liquidación %s del período %s %d.", tipo, MESES[mes], anio);
     }
     private static String getTipoLiquidacion(Integer id_tipo){

        switch(id_tipo){
             case 1:   return  "Mensual";
             case 2:   return  "SAC";
             case 51:  return  "Complementaria";
             default:  return  "Complementaria";
        }

     }
 
  public static void crearDirectorio(String carpeta){
    File directorio = new File(carpeta);                                
    if (!directorio.exists()) {
        System.out.println("Creando directorio: " + carpeta);
        try { // Si no existe el directorio, lo creamos
            directorio.mkdir();
            directorio.setReadable(true);
            directorio.setWritable(true);
        } 
        catch(SecurityException se){ 
            System.err.println("No cuenta con los permisos necesarios para crear el directorio " + carpeta);
        }                        
    }
  }
    private static String descomprimirArchivo(ZipInputStream zis, String outputName ) throws FileNotFoundException, IOException {    
        byte[] buffer = new byte[1024];
        File newFile = new File(outputName);
        FileOutputStream fos = new FileOutputStream(newFile);
        int len;
        while ((len = zis.read(buffer)) > 0) {
        fos.write(buffer, 0, len);
        }
        fos.close();
        return newFile.getAbsolutePath();
    }  
    
    private static void eliminarArchivo(String fileName){
        File archivo = new File(fileName);
        if (archivo.exists()) archivo.delete();
    }
    
    private static String eliminarCeros(String cadena){
        if (cadena == null) return cadena;
        while(cadena.charAt(0) == '0'){
            cadena = cadena.substring(1);
        }
        return cadena;
    }
    public static String repeat(String str, int times) {
        return new String(new char[times]).replace("\0", str);
    }
    public static String getNombreArchivo(String fileEntry){
        String[] splitName   = fileEntry.split("/");
        String nombreArchivo =  splitName.length > 1?(splitName[splitName.length - 1]):fileEntry;
        return nombreArchivo;
    }
}
class FormatException extends Exception {
    public FormatException(String message) {
        super(message);
    }
}