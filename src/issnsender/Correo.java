/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package issnsender;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Diego
 */
public class Correo {
    public String asunto = "";
    public String mensaje = "";
    public List<String> adjuntos = new ArrayList<String>();
    
    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<String> getAdjuntos() {
        return adjuntos;
    }

    public void setAdjuntos(List<String> adjuntos) {
        this.adjuntos = adjuntos;
    }
    public void addAdjunto(String adjunto){
        this.adjuntos.add(adjunto);
    }
    
}
