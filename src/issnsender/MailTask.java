/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package issnsender;


import java.util.concurrent.Callable;
import utilidades.SendMail;

/**
 *
 * @author Diego
 */
public class MailTask implements Callable<Boolean> {
    String mail;
    Correo correo;
    
    public MailTask( String to, Correo correo){
         this.correo = correo;
         this.mail = to;
    }
     
     @Override
    public Boolean call(){
        boolean envioOk = new SendMail("/resources/ss_config.properties").addAttachments(correo.getAdjuntos()).send(this.mail, this.correo.getAsunto(), this.correo.getMensaje());
       // boolean envioOk = (new java.util.Date().getTime() % 2 == 0);
        
        if (!envioOk)             
            System.err.println(String.format("Error al enviar a: " + this.mail));
        return envioOk;
    }
}
