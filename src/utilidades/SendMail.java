/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilidades;

/**
 *
 * @author Diego
 */
// File Name SendMail.java
//import com.sun.mail.smtp.SMTPAddressFailedException;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;

public class SendMail {
    final String email_cuenta;
    final String email_pass;
    final String email_from;
    final String email_smtp;
    final Integer email_port;
    
    protected Properties  props;    
    protected MimeMessage message;
    protected Session     session;
    protected Boolean     debug  = false;
    
    private List<String> attachmentList ;
        
    public static boolean verificaEmail(String email) {
        String emailreg = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
        return email.matches(emailreg);
    }
    
 public static void main(String[] args) {
     new SendMail();
 }
    public SendMail(){        
        this("ss_config.properties");
        
        if (debug) {
            System.out.println(email_cuenta);
            System.out.println(email_pass);
            System.out.println(email_from);
            System.out.println(email_smtp);
            System.out.println(email_port);
        }        
    }
    public SendMail(Boolean debug){
        
        this("ss_config.properties");
        this.debug = debug;
    }
    public SendMail(String propertiesFile){
        if (debug) System.out.println("creating object");
        Properties config = new Properties();

        try {
            //InputStream config_file = getClass().getClassLoader().getResourceAsStream(propertiesFile);
            InputStream config_file = getClass().getResourceAsStream(propertiesFile);
            config.load(config_file);
        } catch (IOException ex) {
            if(debug) System.out.println("Mensaje: " + ex.getMessage());
            Logger.getLogger(SendMail.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        email_cuenta = config.getProperty("email_cuenta");
        email_pass = config.getProperty("email_pass");
        email_from = config.getProperty("email_from");
        email_smtp = config.getProperty("email_smtp");
        email_port = Integer.parseInt(config.getProperty("email_port"));          
        props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", email_smtp);
        props.put("mail.smtp.auth", "true");
        props.put("mail.debug", "false");
        props.put("mail.smtp.port", email_port);
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.ssl.trust", email_smtp);
        
        props.put("mail.smtp.user", email_cuenta); // 
        props.put("mail.smtp.password" , email_pass );
        
//        props.put("mail.transport.protocol", "smtp");
//        props.put("mail.debug", "true"); 
//        props.put("mail.smtp.host", email_smtp); // 
//        props.put("mail.smtp.user", email_cuenta); // 
//        props.put( "mail.smtp.password" , email_pass );
//        props.put("mail.smtp.auth", "true");
//        
//        props.put("mail.smtp.starttls.enable", "true");
//        props.put("mail.smtp.port", email_port);
////        props.put("mail.smtp.socketFactory.port", email_port);
////        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
////        props.put("mail.smtp.socketFactory.fallback", "true");
//        props.put("mail.smtp.ssl.trust", email_smtp);
//        
//        
//        MailSSLSocketFactory sf = null;
//        try {
//            sf = new MailSSLSocketFactory();
//        } catch (GeneralSecurityException e1) {
//            // TODO Auto-generated catch block
//            e1.printStackTrace();
//        }
//        sf.setTrustAllHosts(true);
//        
//        props.put("mail.smtp.ssl.socketFactory", sf);
//        
        session = Session.getDefaultInstance(props);
//        
//        session = Session.getDefaultInstance( props , new Authenticator() {
//        @Override
//                protected PasswordAuthentication getPasswordAuthentication() {
//                  if( email_cuenta == null | email_pass == null ) 
//                      System.out.println("username or password incorrect");
//                  return new PasswordAuthentication( "prueba", "Prueba*123" );
//                }
//    });
//        
        session.setDebug(false);
        attachmentList =  new ArrayList();
    }
    
    public final boolean send(String email, String asunto, String cuerpo) {
        this.create();        
        try {
            if (debug) System.out.println("Sending mail");
            // Quien envia el correo
            message.setFrom(new InternetAddress(email_from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
            message.setSubject(asunto);
            Multipart multipart = new MimeMultipart();
            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();
            // Now set the actual message
            messageBodyPart.setText(cuerpo);
            // Set text message part
            multipart.addBodyPart(messageBodyPart);
            // Part two is attachment
            if (attachmentList.size()>0){
               if (debug) System.out.println("Attachments:" + attachmentList.size());
               for(String filepath:attachmentList){
                    DataSource source = new FileDataSource(filepath);
                    
                    messageBodyPart = new MimeBodyPart();
                    messageBodyPart.setDataHandler(new DataHandler(source));
                    messageBodyPart.setFileName(source.getName());
                    multipart.addBodyPart(messageBodyPart);
               }
            }
            
            // Send the complete message parts
            message.setContent(multipart,"text/html");
            Transport t = session.getTransport("smtp");
            if (debug) System.out.println("Iniciando sesion SMTP");
            t.connect(email_cuenta, email_pass);           
            if (debug) System.out.println("Sesion SMTP iniciada");
            t.sendMessage(message, message.getAllRecipients());
            t.close();
            if (debug) System.out.println("Respuesta: True");
            return true;
        } catch (com.sun.mail.smtp.SMTPAddressFailedException ex) {
           // System.err.println(String.format("Error al enviar el correo a la casilla  ",email));
            return false;
        } catch (MessagingException ex) {
            // System.err.println("CAUSA: " + ex.getCause());
            // System.err.println("Message: " + ex.getMessage());
            // System.err.println("Respuesta: False");
            return false;
        }
    }
    

     public SendMail create(){
         message = new MimeMessage(session);
         return this;
     }
     
     public SendMail addAttachment(String fileName){
         //TODO - Verificar si filename existe
         attachmentList.add(fileName);
         return this;
     }
      public SendMail addAttachments(List<String> files){
         //TODO - Verificar si filename existe
         for(String fileName:files) {
            attachmentList.add(fileName);
         }
         return this;
     }
     public SendMail setAttachment(String fileName){
         //TODO - Verificar si filename existe
         attachmentList.clear();
         attachmentList.add(fileName);
         return this;
     }
}
